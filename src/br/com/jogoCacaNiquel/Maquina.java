package br.com.jogoCacaNiquel;

import java.util.ArrayList;
import java.util.List;

public class Maquina {
    private List<Slot> slots;

    public Maquina(int dificuldade) {
        this.slots = new ArrayList<>();

        for (int i=0; i < dificuldade; i++){
            this.slots.add(new Slot());
        }
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public int calcularPontuacao(){
        int pontuacao = 0;
        for (Slot slot:this.slots) {
            pontuacao = pontuacao + slot.getOpcao().pontos;
        }
        if (existeBonus()){
            pontuacao = pontuacao*10;
        }
        return pontuacao;
    }

    public boolean existeBonus(){
        boolean resp = this.slots.stream().distinct().limit(this.slots.size()).count() == 1;
        return resp;
    }

    @Override
    public String toString() {
        return "Maquina{" +
                "slots=" + slots +
                '}';
    }
}
